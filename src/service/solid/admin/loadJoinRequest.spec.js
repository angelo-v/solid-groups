import data from "@solid/query-ldflex";

import loadJoinRequest from "./loadJoinRequest";
import {list, value, values} from "../../../test-utils/ldflex";

jest.mock("@solid/query-ldflex", () => ({}));

describe("load join request", () => {
    describe("that is a valid request", () => {
        let request;
        beforeEach(async () => {
            data["https://group.example/inbox/request.ttl"] = {
                subjects: values([
                    "https://group.example/inbox/request.ttl#it",
                    "https://group.example/inbox/request.ttl#join",
                ])
            };
            data["https://group.example/inbox/request.ttl#it"] = {
                type: value("https://www.w3.org/ns/activitystreams#Offer"),
                as_actor: value("https://alice.example"),
                as_summary: value("Alice asks to join the group"),
                as_object: value("https://group.example/inbox/request.ttl#join")
            };
            data["https://group.example/inbox/request.ttl#join"] = {
                type: value("https://www.w3.org/ns/activitystreams#Join"),
                as_actor: value("https://alice.example")
            };
            request = await loadJoinRequest(
                "https://group.example/inbox/request.ttl"
            );
        });

        it("uri points to offer", () => {
            expect(request.uri).toBe("https://group.example/inbox/request.ttl#it");
        });

        it("contains summary of offer", () => {
            expect(request.summary).toBe("Alice asks to join the group");
        });

        it("contains a person", () => {
            expect(request.person).toBeDefined();
        });

        it("person's WebID is equal to actor", () => {
            expect(request.person.webId).toBe("https://alice.example");
        });
    });
});
