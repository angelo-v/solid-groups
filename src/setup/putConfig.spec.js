import * as auth from "solid-auth-client";
import { putConfig } from "./putConfig";

jest.mock("solid-auth-client", () => ({
  fetch: jest.fn(),
}));

describe("put config", () => {
  it("resolves on success", async () => {
    auth.fetch.mockResolvedValue({ ok: true });
    const result = await putConfig("https://pod.example/config.js");
    expect(result).toEqual({ ok: true });
  });

  it("rejects on network error", async () => {
    auth.fetch.mockRejectedValue(new Error("network error"));
    const result = putConfig("https://pod.example/config.js");
    await expect(result).rejects.toEqual(new Error("network error"));
  });

  it("rejects on http error", async () => {
    auth.fetch.mockResolvedValue({
      ok: false,
      status: 403,
      statusText: "User unauthorized",
    });
    const result = putConfig("https://pod.example/config.js");
    await expect(result).rejects.toEqual(new Error("User unauthorized"));
  });
});
