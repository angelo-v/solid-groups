const SUCCESS = "success";
const ERROR = "error";

export default (state, { type, payload }) => {
  switch (type) {
    case "ASYNC_STATE_CHANGED": {
      return {
        ...state,
        ...getMessage(state, payload),
      };
    }
    case "CLOSED": {
      return {
        ...state,
        open: false,
      };
    }
    case "SUCCESS_CONFIRMED": {
      return {
        ...state,
        open: true,
        message: payload,
        severity: SUCCESS,
      };
    }
    default: {
      return state;
    }
  }
};

function getMessage(state, { error, value }) {
  if (error) {
    return {
      open: true,
      message: error.message,
      severity: ERROR,
    };
  }
  if (value && value.message) {
    return {
      open: true,
      message: value.message,
      severity: SUCCESS,
    };
  }
  return state;
}
