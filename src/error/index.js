import * as React from "react";
import { useContext, useEffect } from "react";
import { UserFeedback } from "./UserFeedback";
import { useMessage } from "./useMessage";

const FeedbackContext = React.createContext((_) => {
  throw new Error("not implemented");
});

export const Feedback = ({ children }) => {
  const { asyncStateChanged, successConfirmed, open, message, closed, severity } = useMessage();

  return (
    <FeedbackContext.Provider value={{asyncStateChanged, successConfirmed}}>
      <span>
        <UserFeedback
          open={open}
          message={message}
          onClose={closed}
          severity={severity}
        />
        {children}
      </span>
    </FeedbackContext.Provider>
  );
};

export const useUserFeedback = (asyncState) => {
  const {asyncStateChanged, successConfirmed} = useContext(FeedbackContext);
  useEffect(() => {
    asyncStateChanged(asyncState);
  }, [asyncStateChanged, asyncState]);
  return {successConfirmed}
};
