import { toIterablePromise } from "ldflex";

export const value = (string) => ({
  value: Promise.resolve(string),
});

export const values = (list) => ({
  values: Promise.resolve(list),
});

export const missing = () => value(undefined);

export const list = (items) => {
  async function* allItems() {
    while (items.length) {
      const item = items.shift();
      yield value(item);
    }
  }
  return toIterablePromise(allItems);
};

export const emptyList = () => toIterablePromise(async function* empty() {});
