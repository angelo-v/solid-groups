import data from "@solid/query-ldflex";

export default async (webId) => {
  const name = await getName(webId);
  const imageSrc = await getImageSrc(webId);
  const inbox = await data[webId].ldp_inbox.value;
  return { webId, name, inbox, imageSrc };
};

async function getName(webId) {
  const vcard = await data[webId].vcard_fn.value;
  const foaf = !vcard && (await data[webId].foaf_name.value);
  return vcard || foaf;
}

async function getImageSrc(webId) {
  const vcard = await data[webId].vcard_hasPhoto.value;
  const foaf = !vcard && (await data[webId].foaf_img.value);
  return vcard || foaf;
}
