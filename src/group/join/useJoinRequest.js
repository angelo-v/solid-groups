import {useServiceContext} from "../../service";
import {useSession} from "../../authentication";
import {useAsyncFn} from "react-use";
import {useUserFeedback} from "../../error";

export const useJoinRequest = (group) => {
  const { sendJoinRequest } = useServiceContext();
  const { user } = useSession();

  const [requestState, joinRequested] = useAsyncFn( async () => {
    await sendJoinRequest(group, user);
    return {
      message: "Your join request has been sent."
    }
  },[user, group])

  useUserFeedback(requestState)

  return {
    joinRequested,
  };
};
