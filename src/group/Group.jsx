import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import {Hero} from "../layout/Hero";
import {Main} from "../layout/Main";
import {Sidebar} from "../layout/Sidebar";
import {useGroup} from "./useGroup";
import LinearProgress from "@material-ui/core/LinearProgress";
import {PageError} from "../error/PageError";
import {Layout} from "../layout/Layout";
import {Members} from "./members/Members";
import {Membership} from "./membership/Membership";

const useStyles = makeStyles((theme) => ({
  mainGrid: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
  },
}));

export const PageLoading = () => <LinearProgress color="secondary" />;

export const GroupPage = ({ group }) => {
  const classes = useStyles();
  return (
    <Layout>
      <Hero title={group.name} description="A Solid group." />
      <Grid container spacing={5} className={classes.mainGrid}>
        <Main title="Members">
          <Members group={group} />
        </Main>
        <Sidebar title="" description="">
          <Membership group={group} />
        </Sidebar>
      </Grid>
    </Layout>
  );
};

export const Group = ({ uri }) => {
  const { group, loading, error } = useGroup(uri);

  if (loading) return <PageLoading />;
  if (error) return <PageError error={error} />;

  return <GroupPage group={group} />;
};
