import {useSetupGroupForm} from "./useSetupGroupForm";
import {act} from "react-dom/test-utils";
import {putConfig} from "./putConfig";
import {initializeGroupUsing} from "./initializeGroup";
import {withHook} from "../test-utils";

jest.mock("./putConfig");
jest.mock("./initializeGroup");
jest.mock("../error");

describe("useSetupGroupForm", () => {
  let render;
  let renderCycle;

  const { location } = window;

  beforeEach(() => {
    delete window.location;
    window.location = {
      reload: jest.fn(),
    };
  });

  afterEach(() => {
    window.location = location;
  });

  let initializeGroup;

  beforeEach(() => {
    jest.resetAllMocks();
    initializeGroup = jest.fn();
    initializeGroupUsing.mockReturnValue(initializeGroup);
    ({render, renderCycle} = withHook(useSetupGroupForm));
  });

  describe("uri change", () => {
    const INITIAL = 0;
    const AFTER_URI_CHANGE = 1;

    it("returns initial group uri value based on host", () => {
      window.location = {
        protocol: "https:",
        host: "group-pod.example",
      };
      render();
      expect(renderCycle[INITIAL].uri).toEqual(
        "https://group-pod.example/groups/main#we"
      );
    });

    it("returns new uri after change", () => {
      render();
      act(() => {
        renderCycle[INITIAL].onUriChange("https://other.example/");
      });
      expect(renderCycle[AFTER_URI_CHANGE].uri).toEqual(
        "https://other.example/"
      );
    });
  });

  describe("on submit", () => {
    const INITIAL = 0;
    const AFTER_URI_CHANGE = 1;
    const AFTER_SUBMIT_TRIGGERED = 2;
    const AFTER_SUBMIT_FINISHED = 3;

    describe("successfully", () => {
      beforeEach(async () => {
        render();
        act(() => {
          renderCycle[0].onUriChange("https://uri.example/");
        });
        await act(async () => {
          await renderCycle[1].onSubmit();
        });
      });

      it("should initialize group", async () => {
        expect(initializeGroup).toHaveBeenCalledWith("https://uri.example/");
      });

      it("should put config after initializing group", async () => {
        expect(putConfig).toHaveBeenCalledWith("https://uri.example/");
      });

      it("should reload page", async () => {
        expect(window.location.reload).toHaveBeenCalledWith();
      });

      it("submit state shows progress", () => {
        expect(renderCycle[INITIAL].submitState).toEqual({
          loading: false,
        });
        expect(renderCycle[AFTER_URI_CHANGE].submitState).toEqual({
          loading: false,
        });
        expect(renderCycle[AFTER_SUBMIT_TRIGGERED].submitState).toEqual({
          loading: true,
        });
        expect(renderCycle[AFTER_SUBMIT_FINISHED].submitState).toEqual({
          loading: false,
          value: {
            message: "Congrats! Your group has been set up."
          }
        });
      });
    });

    describe("error initializing group", () => {
      beforeEach(async () => {
        initializeGroup.mockRejectedValue(new Error("failed to init group"));
        render();
        act(() => {
          renderCycle[INITIAL].onUriChange("https://uri.example/");
        });
        await act(async () => {
          await renderCycle[AFTER_URI_CHANGE].onSubmit();
        });
      });

      it("should initialize group", async () => {
        expect(initializeGroup).toHaveBeenCalledWith("https://uri.example/");
      });

      it("should not put config", async () => {
        expect(putConfig).not.toHaveBeenCalled();
      });

      it("should not reload page", async () => {
        expect(window.location.reload).not.toHaveBeenCalledWith();
      });

      it("submit state shows progress", () => {
        expect(renderCycle[INITIAL].submitState).toEqual({
          loading: false,
        });
        expect(renderCycle[AFTER_URI_CHANGE].submitState).toEqual({
          loading: false,
        });
        expect(renderCycle[AFTER_SUBMIT_TRIGGERED].submitState).toEqual({
          loading: true,
        });
        expect(renderCycle[AFTER_SUBMIT_FINISHED].submitState).toEqual({
          loading: false,
          error: new Error("failed to init group"),
        });
      });
    });

    describe("error putting config", () => {
      beforeEach(async () => {
        putConfig.mockRejectedValue(new Error("failed to put config"));
        render();
        act(() => {
          renderCycle[0].onUriChange("https://uri.example/");
        });
        await act(async () => {
          await renderCycle[1].onSubmit();
        });
      });

      it("should initialize group", async () => {
        expect(initializeGroup).toHaveBeenCalledWith("https://uri.example/");
      });

      it("should try to put config", async () => {
        expect(putConfig).toHaveBeenCalledWith("https://uri.example/");
      });

      it("should not reload page", async () => {
        expect(window.location.reload).not.toHaveBeenCalledWith();
      });

      it("submit state shows progress", () => {
        expect(renderCycle[INITIAL].submitState).toEqual({
          loading: false,
        });
        expect(renderCycle[AFTER_URI_CHANGE].submitState).toEqual({
          loading: false,
        });
        expect(renderCycle[AFTER_SUBMIT_TRIGGERED].submitState).toEqual({
          loading: true,
        });
        expect(renderCycle[AFTER_SUBMIT_FINISHED].submitState).toEqual({
          loading: false,
          error: new Error("failed to put config"),
        });
      });
    });
  });
});
