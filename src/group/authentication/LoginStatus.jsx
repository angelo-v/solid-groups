import React from "react";
import { useSession } from "../../authentication";
import { Login } from "./Login";
import { UserInfo } from "./UserInfo";

export const LoginStatus = () => {
  const { loggedIn, user } = useSession();
  return loggedIn ? <UserInfo {...user} /> : <Login />;
};
