import data from "@solid/query-ldflex";

export default async (person, group) => {
  await data[group.uri].vcard_hasMember.add(data[person.webId]);
};
