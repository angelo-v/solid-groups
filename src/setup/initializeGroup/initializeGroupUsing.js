
export default ({createGroup, createInbox, loadPotentialGroup}) => async (uri) => {
  const { exists, isGroup, hasInbox } = await loadPotentialGroup(uri);
  if (exists && !isGroup) {
    throw new Error("the resource exists, but is not a group");
  }
  if (!exists) {
    await createGroup(uri);
  }
  if (!hasInbox) {
    await createInbox(uri);
  }
};
