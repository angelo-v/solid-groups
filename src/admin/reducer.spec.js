import reducer, { deleted } from "./reducer";

describe("join requests reducer", () => {
  it("delted join request gets removed", () => {
    const state = reducer(
      [
        {
          uri: "https://group.example/inbox/request/1",
        },
        {
          uri: "https://group.example/inbox/request/2",
        },
        {
          uri: "https://group.example/inbox/request/3",
        },
      ],
      deleted({ uri: "https://group.example/inbox/request/2" })
    );
    expect(state).toEqual([
      {
        uri: "https://group.example/inbox/request/1",
      },

      {
        uri: "https://group.example/inbox/request/3",
      },
    ]);
  });
});
