import * as auth from "solid-auth-client";
import { trackSession } from "./index";
import { loadProfile } from "../person";

const noop = () => null;

jest.mock("../person");

jest.mock("solid-auth-client", () => ({
  trackSession: jest.fn(),
}));

describe("track session", () => {
  let sessionChange;
  beforeEach(() => {
    jest.resetAllMocks();
    auth.trackSession.mockImplementation(
      (callback) => (sessionChange = callback)
    );
  });

  it("calls onLogin with user profile after login", async () => {
    const user = { name: "Jane" };
    loadProfile.mockResolvedValue(user);

    const onLogin = jest.fn();
    trackSession(onLogin, noop);
    await sessionChange({ webId: "webId" });

    expect(onLogin).toHaveBeenCalledWith(user);
    expect(loadProfile).toHaveBeenCalledWith("webId");
  });

  it("calls onLogout after logout", async () => {
    const onLogout = jest.fn();
    trackSession(noop, onLogout);
    await sessionChange(null);

    expect(onLogout).toHaveBeenCalled();
    expect(loadProfile).not.toHaveBeenCalled();
  });
});
