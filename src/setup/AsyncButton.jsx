import Button from "@material-ui/core/Button";
import React, { useEffect } from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import { useTimeout } from "react-use";

export const AsyncButton = ({ children, loading, timeout = 300, ...props }) => {
  const [showLoading, cancel, reset] = useTimeout(timeout);

  useEffect(() => {
    if (loading) {
      reset();
    } else {
      cancel();
    }
  }, [cancel, reset, loading]);

  return (
    <Button {...props}>
      {loading && showLoading() ? (
        <CircularProgress color="secondary" size="2rem" />
      ) : (
        children
      )}{" "}
    </Button>
  );
};
