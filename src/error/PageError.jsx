import React from "react";
import Container from "@material-ui/core/Container";
import Avatar from "@material-ui/core/Avatar";
import ErrorIcon from '@material-ui/icons/Error';
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.error.main,
    },
}));

export const PageError = ({error}) => {
    const classes = useStyles();
    return (
        <Container maxWidth="xs">
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <ErrorIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Sorry, something went wrong.
                </Typography>
                <pre><code>{error.toString()}</code></pre>
            </div>
        </Container>
    );
};
