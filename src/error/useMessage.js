import { useCallback, useReducer } from "react";
import reducer from "./reducer";

const errorReport = `
This might be a bug in Solid Groups.
Please report an issue at

https://gitlab.com/angelo-v/solid-groups/-/issues

and include the following error details:

`;
const banner = `
***************************************************************************
`;

function log(error) {
  if (error) {
    console.error(banner, errorReport, error, banner);
  }
}

export const useMessage = () => {
  const [state, dispatch] = useReducer(reducer, {
    open: false,
  });

  const asyncStateChanged = useCallback((asyncState) => {
    log(asyncState.error);
    dispatch({ type: "ASYNC_STATE_CHANGED", payload: asyncState });
  }, []);

  return {
    ...state,
    asyncStateChanged,
    successConfirmed: (message) =>
      dispatch({ type: "SUCCESS_CONFIRMED", payload: message }),
    closed: () => dispatch({ type: "CLOSED" }),
  };
};
