import React from "react";
import {Redirect} from "react-router-dom";

import config from './config'
import {GroupRoutes} from "./GroupRoutes";

export function Main() {
  return !config.uri ? <Redirect to="/setup"/> : <GroupRoutes uri={config.uri}/>;
}
