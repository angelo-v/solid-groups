import { act } from "react-dom/test-utils";
import { useServiceContext } from "../service";
import { useSession } from "../authentication";
import { withHook } from "../test-utils";
import { useUserFeedback } from "../error";
import { useJoinAcceptance } from "./useJoinAcceptance";

jest.mock("../authentication");
jest.mock("../service");
jest.mock("../error");

describe("use join acceptance", () => {
  let render, renderCycle;
  let sendJoinAcceptance, loadProfile, deleteJoinRequest, successConfirmed, onAccepted, addToGroup;
  let request, session;
  beforeEach(() => {
    jest.resetAllMocks();
    sendJoinAcceptance = jest.fn();
    loadProfile = jest.fn();
    successConfirmed = jest.fn();
    onAccepted = jest.fn();
    deleteJoinRequest = jest.fn();
    addToGroup = jest.fn();
    useServiceContext.mockReturnValue({ sendJoinAcceptance, loadProfile, deleteJoinRequest, addToGroup});
    useUserFeedback.mockReturnValue({ successConfirmed });
    // given an admin user
    session = { user: { name: "Admin" } };
    useSession.mockReturnValue(session);
    // and a group
    const group = {
      uri: "https://group.example/group/main#we",
    };
    // and a person wanting to join
    const person = { name: "Jane" };
    loadProfile.mockResolvedValue(person);
    request = { uri: "https://group.example/inbox/request.ttl", group, person };
    ({ render, renderCycle } = withHook(
      useJoinAcceptance,
      request,
      onAccepted
    ));
  });

  describe("when a join is accepted successfully", () => {
    const INITIAL = 0;
    const JOIN_ACCEPTED = 1;

    beforeEach(async () => {
      sendJoinAcceptance.mockResolvedValue("ok");
      render();
      await act(async () => {
        await renderCycle[INITIAL].joinAccepted();
      });
    });

    it("does not indicate loading initially", () => {
      expect(useUserFeedback.mock.calls[INITIAL][0]).toEqual({
        loading: false,
      });
    });

    it("sends a join requests", () => {
      expect(sendJoinAcceptance).toHaveBeenCalledWith(
        session.user,
        request.group,
        request.person
      );
    });

    it("indicates loading while request is sent", () => {
      expect(useUserFeedback.mock.calls[JOIN_ACCEPTED][0]).toEqual({
        loading: true,
      });
    });

    it("deletes the join request", () => {
      expect(deleteJoinRequest).toHaveBeenCalledWith(request);
    });

    it("calls onAccepted callback", () => {
      expect(onAccepted).toHaveBeenCalledWith(request);
    });

    it("adds the person to the group", () => {
      expect(addToGroup).toHaveBeenCalledWith(request.person, request.group);
    });

    it("indicates success after request finished", () => {
      expect(successConfirmed).toHaveBeenCalledWith(
        "You accepted the group join."
      );
    });
  });

  describe("when loading fails", () => {
    const INITIAL = 0;
    const JOIN_ACCEPTED = 1;
    const ACCEPTANCE_FINISHED = 2;

    beforeEach(async () => {
      sendJoinAcceptance.mockRejectedValue(new Error("oh oh"));
      render();
      await act(async () => {
        await renderCycle[INITIAL].joinAccepted();
      });
    });

    it("does not indicate loading initially", () => {
      expect(useUserFeedback.mock.calls[INITIAL][0]).toEqual({
        loading: false,
      });
    });

    it("indicates loading while request is sent", () => {
      expect(useUserFeedback.mock.calls[JOIN_ACCEPTED][0]).toEqual({
        loading: true,
      });
    });

    it("indicates error after request finished", () => {
      expect(useUserFeedback.mock.calls[ACCEPTANCE_FINISHED][0]).toEqual({
        loading: false,
        error: new Error("oh oh"),
      });
    });
  });
});
