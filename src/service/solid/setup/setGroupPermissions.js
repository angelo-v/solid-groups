import * as auth from "solid-auth-client";
import {AclApi, Agents, Permissions} from "solid-acl-utils";

export default async (groupUri) => {
    const groupDocUrl = groupUri.replace(/#.*/, "")
    const fetch = auth.fetch.bind(auth);
    const aclApi = new AclApi(fetch, { autoSave: true });
    const acl = await aclApi.loadFromFileUrl(groupDocUrl);
    await acl.addRule(Permissions.READ, Agents.PUBLIC);
}
