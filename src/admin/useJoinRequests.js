import { useServiceContext } from "../service";
import reducer, {deleted} from "./reducer";
import useAsyncReducer from "./useAsyncReducer";

export function useJoinRequests({ uri }) {
  const { loadJoinRequests } = useServiceContext();

  const [state, dispatch] = useAsyncReducer(
    reducer,
    [],
    () => loadJoinRequests(uri),
    [loadJoinRequests, uri]
  );

  return {
    ...state,
    rejected: (request) => dispatch(deleted(request)),
    accepted: (request) => dispatch(deleted(request)),
    requests: state.data,
  };
}
