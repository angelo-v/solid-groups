import React from "react";
import { useGroup } from "./useGroup";
import { shallow } from "enzyme";
import { Group, GroupPage, PageLoading } from "./Group";
import { PageError } from "../error/PageError";

jest.mock("./useGroup");

describe("Group", () => {
  it("renders the group after loading successfully", () => {
    useGroup.mockReturnValue({
      loading: false,
      error: false,
      group: {
        name: "test group",
      },
    });
    const result = shallow(<Group />);
    expect(result).toContainReact(<GroupPage group={{ name: "test group" }} />);
  });

  it("renders loading indicator while loading", () => {
    useGroup.mockReturnValue({
      loading: true,
      error: false,
    });
    const result = shallow(<Group />);
    expect(result).toContainReact(<PageLoading />);
  });

  it("renders error indicator when loading failed", () => {
    const error = new Error("oops");
    useGroup.mockReturnValue({
      loading: false,
      error,
    });
    const result = shallow(<Group />);
    expect(result).toContainReact(<PageError error={error} />);
  });
});
