import { loadGroup } from "./loadGroup";
import data from "@solid/query-ldflex";
import { missing, value, values } from "../../../test-utils/ldflex";

jest.mock("@solid/query-ldflex", () => ({}));

describe("when loading a group", () => {
  describe("successfully with all data available", () => {
    let group;
    beforeEach(async () => {
      data["https://group.example/group/1"] = {
        vcard_fn: value("My awesome group"),
        ldp_inbox: value("https://group.example/inbox/1"),
        vcard_hasMember: values([
          "https://alice.example#me",
          "https://bob.example#me",
          "https://claire.example#me",
        ]),
      };
      group = await loadGroup("https://group.example/group/1");
    });

    it("the result contains the uri", () => {
      expect(group.uri).toBe("https://group.example/group/1");
    });

    it("the result contains the name", () => {
      expect(group.name).toBe("My awesome group");
    });

    it("the result contains the inbox", () => {
      expect(group.inbox).toBe("https://group.example/inbox/1");
    });

    it("the result contains all members", () => {
      expect(group.members).toEqual([
        { webId: "https://alice.example#me" },
        { webId: "https://bob.example#me" },
        { webId: "https://claire.example#me" },
      ]);
    });
  });

  describe("successfully, but with missing data", () => {
    let group;
    beforeEach(async () => {
      data["https://group.example/group/1"] = {
        vcard_fn: missing(),
        ldp_inbox: missing(),
        vcard_hasMember: values([]),
      };
      group = await loadGroup("https://group.example/group/1");
    });

    it("the result contains the uri", () => {
      expect(group.uri).toBe("https://group.example/group/1");
    });

    it("the result contains no name", () => {
      expect(group.name).toBeUndefined();
    });

    it("the result contains no inbox", () => {
      expect(group.inbox).toBeUndefined();
    });

    it("the result has no members", () => {
      expect(group.members).toEqual([]);
    });
  });
});
