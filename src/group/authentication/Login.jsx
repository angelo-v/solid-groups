import Button from "@material-ui/core/Button";
import React from "react";
import {useSession} from "../../authentication";

export const Login = ({variant = "outlined", color}) => {
    const {loggedIn, login} = useSession();
    return !loggedIn && <Button variant={variant} color={color} size="small" onClick={login}>
        Log in
    </Button>;
}