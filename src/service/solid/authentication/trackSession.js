import * as auth from "solid-auth-client";

import { loadProfile } from "../person";

export default (onLogin, onLogout) =>
  auth.trackSession(async (session) => {
    if (!session) {
      onLogout();
    } else {
      const user = await loadProfile(session.webId);
      onLogin(user);
    }
  });
