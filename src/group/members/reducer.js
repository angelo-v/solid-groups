export default (state, { type, payload }) => {
  switch (type) {
    case "PROFILE_LOADING": {
      return state.map((member) => {
        if (member.webId === payload.webId) {
          return { ...member, loading: true };
        }
        return member;
      });
    }
    case "PROFILE_LOADED": {
      return state.map((member) => {
        if (member.webId === payload.webId) {
          return { ...member, ...payload, loading: false, ready: true };
        }
        return member;
      });
    }
    default: {
      return state;
    }
  }
};
