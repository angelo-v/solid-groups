import * as auth from "solid-auth-client";
import {AclApi, Agents, Permissions} from "solid-acl-utils";

export default async (inboxUrl) => {
    const fetch = auth.fetch.bind(auth);
    const aclApi = new AclApi(fetch, { autoSave: false });
    const acl = await aclApi.loadFromFileUrl(inboxUrl);
    await addPublicAppendPermission(acl);
    await addFullDefaultAccessForOwners(acl);
    await acl.saveToPod();
}

function addPublicAppendPermission(acl) {
    return acl.addRule(Permissions.APPEND, Agents.PUBLIC);
}

async function addFullDefaultAccessForOwners(acl) {
    const ownerPermissions = [Permissions.READ, Permissions.WRITE, Permissions.CONTROL]
    const owners = acl.getAgentsWith(ownerPermissions);
    await acl.addDefaultRule(ownerPermissions, owners);
}