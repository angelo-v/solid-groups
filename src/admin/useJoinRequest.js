import { useJoinRejection } from "./useJoinRejection";
import { useJoinAcceptance } from "./useJoinAcceptance";

export const useJoinRequest = (request, onAccepted, onRejected) => {
  const { joinRejected } = useJoinRejection(request, onRejected);
  const { joinAccepted } = useJoinAcceptance(request, onAccepted);
  return {
    joinAccepted,
    joinRejected,
  };
};
