import { useEffect, useReducer } from "react";
import reducer from "./reducer";
import { useServiceContext } from "../../service";

export const useMembers = (group) => {
  const { loadProfile } = useServiceContext();
  const [members, dispatch] = useReducer(reducer, group.members);

  useEffect(() => {
    group.members.forEach((it) => {
      dispatch({ type: "PROFILE_LOADING", payload: it });
      loadProfile(it.webId).then((profile) => {
        dispatch({ type: "PROFILE_LOADED", payload: profile });
      });
    });
  }, [group.members, loadProfile]);

  return {
    members,
  };
};
