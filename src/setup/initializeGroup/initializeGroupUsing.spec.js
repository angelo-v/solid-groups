import initializeGroupUsing from "./initializeGroupUsing";

describe("initialize group using", () => {

  let createGroup;
  let createInbox;
  let loadPotentialGroup;
  let initializeGroup;

  beforeEach(() => {
    createGroup = jest.fn();
    createInbox = jest.fn();
    loadPotentialGroup = jest.fn();
    initializeGroup = initializeGroupUsing({createGroup, createInbox, loadPotentialGroup})
  });

  describe("from scratch", () => {
    beforeEach(async () => {
      loadPotentialGroup.mockReturnValue({
        exists: false,
      });
      await initializeGroup("https://group.example#we");
    });

    it("creates the group", () => {
      expect(createGroup).toHaveBeenCalledWith("https://group.example#we");
    });

    it("creates an inbox", () => {
      expect(createInbox).toHaveBeenCalledWith("https://group.example#we");
    });
  });

  describe("that already exists, but does not have inbox", () => {
    beforeEach(async () => {
      loadPotentialGroup.mockReturnValue({
        exists: true,
        isGroup: true,
        hasInbox: false,
      });
      await initializeGroup("https://group.example#we");
    });

    it("does not create the group", () => {
      expect(createGroup).not.toHaveBeenCalled();
    });

    it("but creates an inbox", () => {
      expect(createInbox).toHaveBeenCalledWith("https://group.example#we");
    });
  });

  describe("that is already fully set up", () => {
    beforeEach(async () => {
      loadPotentialGroup.mockReturnValue({
        exists: true,
        isGroup: true,
        hasInbox: true,
      });
      await initializeGroup("https://group.example#we");
    });

    it("does not create the group", () => {
      expect(createGroup).not.toHaveBeenCalled();
    });

    it("and does not create an inbox", () => {
      expect(createInbox).not.toHaveBeenCalled();
    });
  });

  describe("for an URI of a non-group resource", () => {
    beforeEach(async () => {
      loadPotentialGroup.mockReturnValue({
        exists: true,
        isGroup: false,
      });
    });

    it("throws an error instead of creating a group", async () => {
      await expect(initializeGroup()).rejects.toThrow(
        "the resource exists, but is not a group"
      );
      expect(createGroup).not.toHaveBeenCalled();
    });
  });
});
