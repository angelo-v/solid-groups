import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Logout } from "./Logout";
import { useSetupGroupForm } from "./useSetupGroupForm";
import { AsyncButton } from "./AsyncButton";

const useStyles = makeStyles((theme) => ({
  form: {
    width: "100%",
    marginTop: theme.spacing(1),
  },
  submit: {
    height: "3rem",
    fontSize: "large",
    margin: theme.spacing(3, 0, 2),
  },
}));

export const SetupGroupForm = () => {
  const classes = useStyles();
  const { uri, onUriChange, onSubmit, submitState } = useSetupGroupForm();
  return (
    <>
      <form
        className={classes.form}
        onSubmit={(event) => {
          onSubmit();
          event.preventDefault();
        }}
      >
        <TextField
          type="url"
          variant="outlined"
          margin="normal"
          required
          fullWidth
          id="uri"
          label="Group URI"
          name="uri"
          autoFocus
          onChange={(event) => onUriChange(event.target.value)}
          value={uri}
        />
        <Typography variant="body2">
          Specify the URI of a group you want to manage using this app. If no
          group is present at this URI, it will be created.
        </Typography>
        <AsyncButton
          type="submit"
          fullWidth
          variant="contained"
          color="primary"
          className={classes.submit}
          loading={submitState.loading}
        >
          Set up
        </AsyncButton>
        <Logout />
      </form>
    </>
  );
};
