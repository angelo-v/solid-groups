import React from "react";
import { mount } from "enzyme";

export default (hook, ...hookArgs) => {
  const renderCycle = [];
  const TestComponent = ({ state }) => {
    const result = state ? hook(state, ...hookArgs) : hook(...hookArgs);
    renderCycle.push(result);
    return <></>;
  };
  const render = (state) => mount(<TestComponent state={state} />);
  return { render, renderCycle };
}
