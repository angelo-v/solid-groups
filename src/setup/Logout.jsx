import Grid from "@material-ui/core/Grid";
import React from "react";
import { useSession } from "../authentication";
import Avatar from "@material-ui/core/Avatar";
import Tooltip from "@material-ui/core/Tooltip";
import Chip from "@material-ui/core/Chip";
import LogoutIcon from "@material-ui/icons/ExitToApp";

export const Logout = () => {
  const { user, logout } = useSession();
  return (
    <Grid container>
      <Grid item xs>
        <Tooltip title={user.webId}>
          <Chip
            variant="outlined"
            avatar={<Avatar alt={user.name} src={user.imageSrc} />}
            label={user.name}
            deleteIcon={
              <Tooltip title={"Log out"}>
                <LogoutIcon />
              </Tooltip>
            }
            onDelete={logout}
          />
        </Tooltip>
      </Grid>
    </Grid>
  );
};
