import reducer from "./reducer";

describe("user feedback reducer", () => {
  describe("when async state changed", () => {
    describe("to error", () => {
      let newState;
      beforeEach(() => {
        const currentState = {
          open: false,
          message: "Worked well.",
          severity: "success",
        };
        const action = {
          type: "ASYNC_STATE_CHANGED",
          payload: {
            loading: false,
            error: new Error("That crashed."),
          },
        };
        newState = reducer(currentState, action);
      });
      it("then it opens", () => {
        expect(newState.open).toBe(true);
      });
      it("shows the error messagse", () => {
        expect(newState.message).toBe("That crashed.");
      });
      it("has error severity", () => {
        expect(newState.severity).toBe("error");
      });
    });
    describe("to success with message", () => {
      let newState;
      beforeEach(() => {
        const currentState = {
          open: false,
          message: "That crashed.",
          severity: "error",
        };
        const action = {
          type: "ASYNC_STATE_CHANGED",
          payload: {
            loading: false,
            value: {
              message: "Worked well.",
            },
          },
        };
        newState = reducer(currentState, action);
      });
      it("then it opens", () => {
        expect(newState.open).toBe(true);
      });
      it("shows the success message", () => {
        expect(newState.message).toBe("Worked well.");
      });
      it("has success severity", () => {
        expect(newState.severity).toBe("success");
      });
    });
    describe("to success without message", () => {
      let newState;
      beforeEach(() => {
        const currentState = {
          open: false,
          message: "That crashed.",
          severity: "error",
        };
        const action = {
          type: "ASYNC_STATE_CHANGED",
          payload: {
            loading: false,
            value: 42,
          },
        };
        newState = reducer(currentState, action);
      });
      it("then it stays closed", () => {
        expect(newState.open).toBe(false);
      });
    });
    describe("to success without message when already opened", () => {
      let newState;
      let currentState
      beforeEach(() => {
        currentState = {
          open: true,
          message: "That went well.",
          severity: "success",
        };
        const action = {
          type: "ASYNC_STATE_CHANGED",
          payload: {
            loading: false,
            value: 42,
          },
        };
        newState = reducer(currentState, action);
      });
      it("then it keeps all data", () => {
        expect(newState).toEqual(currentState);
      });
    });
  });

  describe("when closed", () => {
    let newState;
    beforeEach(() => {
      const currentState = {
        open: true,
        message: "Something is shown.",
        severity: "whatever",
      };
      const action = {
        type: "CLOSED",
      };
      newState = reducer(currentState, action);
    });
    it("then it closes", () => {
      expect(newState.open).toBe(false);
    });
  });

  describe("when success confirmed", () => {
    let newState;
    beforeEach(() => {
      const currentState = {
        open: false,
        message: "Some old message.",
        severity: "whatever",
      };
      const action = {
        type: "SUCCESS_CONFIRMED",
        payload: "You did very well",
      };
      newState = reducer(currentState, action);
    });
    it("then it opens", () => {
      expect(newState.open).toBe(true);
    });
    it("and shows the message", () => {
      expect(newState.message).toBe("You did very well");
    });
    it("in success severity", () => {
      expect(newState.severity).toBe("success");
    });
  });
});
