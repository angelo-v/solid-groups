import React from "react";
import { shallow } from "enzyme";

import { JoinGroup } from "./JoinGroup";
import Button from "@material-ui/core/Button";
import { useSession } from "../../authentication";
import { useJoinRequest } from "./useJoinRequest";

jest.mock("../../authentication");
jest.mock("./useJoinRequest");

describe("JoinGroup", () => {
  it("should trigger onJoinRequest when join button is clicked", () => {
    const joinRequested = jest.fn();
    useSession.mockReturnValue({ loggedIn: true });
    useJoinRequest.mockReturnValue({
      joinRequested,
    });
    const result = shallow(<JoinGroup />);
    result.find(Button).simulate("click");
    expect(joinRequested).toHaveBeenCalled();
  });
});
