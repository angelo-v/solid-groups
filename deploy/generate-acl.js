const Handlebars = require("handlebars");
const fs = require("fs");
const path = require("path");

const aclTemplates = {
  staticFolder: `
# ACL for the static folder
# Public-readable

@prefix acl:  <http://www.w3.org/ns/auth/acl#> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .

<#owner>
    a acl:Authorization ;
    acl:agent <{{owner}}> ;
    acl:accessTo <./> ;
    acl:default <./> ;
    acl:mode acl:Read, acl:Write, acl:Control .

<#public>
    a acl:Authorization ;
    acl:agentClass foaf:Agent ;
    acl:accessTo <./> ;
    acl:default <./> ;
    acl:mode acl:Read .
`,
  file: (name) => `
# ACL for the default manifest.json resource
# Public-readable

@prefix acl:  <http://www.w3.org/ns/auth/acl#> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .

<#owner>
    a acl:Authorization ;
    acl:agent <{{owner}}> ;
    acl:accessTo <${name}> ;
    acl:mode acl:Read, acl:Write, acl:Control .

<#public>
    a acl:Authorization ;
    acl:agentClass foaf:Agent ;
    acl:accessTo <${name}> ;
    acl:mode acl:Read .
`,
};

const staticFolderAclFilename = path.join(
  process.cwd(),
  "build",
  "static",
  ".acl"
);

const aclFilePathOfFile = (name) =>
  path.join(process.cwd(), "build", `${name}.acl`);

function writeAcl(templateData, filename) {
  const aclFilename = aclFilePathOfFile(filename);
  const acl = Handlebars.compile(aclTemplates.file(filename))(templateData);
  console.log("writing file", aclFilename);
  fs.writeFileSync(aclFilename, acl);
}

module.exports = (webId) => {
  const templateData = { owner: webId };
  console.log(
    "generating ACL files with owner",
    webId,
    "granting public read access"
  );
  const staticFolderAcl = Handlebars.compile(aclTemplates.staticFolder)(
    templateData
  );
  console.log("writing file", staticFolderAclFilename);
  fs.writeFileSync(staticFolderAclFilename, staticFolderAcl);

  [
    "manifest.json",
    "favicon.ico",
    "logo192.png",
    "logo512.png",
    "config.js",
  ].forEach((filename) => writeAcl(templateData, filename));
};
