import React from "react";
import GroupAddIcon from "@material-ui/icons/GroupAdd";
import Button from "@material-ui/core/Button";
import { useJoinRequest } from "./useJoinRequest";

export function JoinGroup({ group }) {
  const { joinRequested } = useJoinRequest(group);
  return (
    <Button
      onClick={() => joinRequested()}
      startIcon={<GroupAddIcon />}
      variant="contained"
      color="secondary"
    >
      Join Group
    </Button>
  );
}
