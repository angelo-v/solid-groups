import { useAsyncFn } from "react-use";
import { useServiceContext } from "../service";
import { useSession } from "../authentication";
import { useUserFeedback } from "../error";

export const useJoinRejection = (request, onRejected) => {
  const {
    sendJoinRejection,
    loadProfile,
    deleteJoinRequest,
  } = useServiceContext();
  const { user: admin } = useSession();

  const [rejectionState, joinRejected] = useAsyncFn(async () => {
    const profile = await loadProfile(request.person.webId);
    await sendJoinRejection(admin, request.group, profile);
    await deleteJoinRequest(request);
    onRejected(request);
    successConfirmed("You rejected the group join.");
  }, [admin, request, onRejected]);

  const { successConfirmed } = useUserFeedback(rejectionState);

  return {
    joinRejected,
  };
};
