import data from "@solid/query-ldflex";
import loadPotentialGroup from "./loadPotentialGroup";
import { createInbox } from "./inbox";
import setGroupPermissions from "./setGroupPermissions";

const vcardGroup = data["http://www.w3.org/2006/vcard/ns#Group"];
const asGroup = data["https://www.w3.org/ns/activitystreams#Group"];

const createGroup = async (uri) => {
    await data[uri].type.add(vcardGroup, asGroup);
    await setGroupPermissions(uri);
};

export { loadPotentialGroup, createGroup, createInbox };
