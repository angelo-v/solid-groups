import { useSession } from "../../authentication";
import { useMembership } from "./useMembership";

jest.mock("../../authentication");

describe("use membership", () => {
  describe("when user's WebID is in the members list", () => {
    let result;
    beforeEach(() => {
      useSession.mockReturnValue({ loggedIn: true, user: { webId: "alice" } });
      const group = {
        members: [{ webId: "alice" }],
      };
      result = useMembership(group);
    });
    it("the user is logged in", () => {
      expect(result.loggedIn).toBe(true);
    });
    it("the user is part of the group", () => {
      expect(result.userIsMember).toBe(true);
    });
  });

  describe("when user's WebID is not in the members list", () => {
    let result;
    beforeEach(() => {
      useSession.mockReturnValue({ loggedIn: true, user: { webId: "alice" } });
      const group = {
        members: [{ webId: "bob" }],
      };
      result = useMembership(group);
    });
    it("the user is logged in", () => {
      expect(result.loggedIn).toBe(true);
    });
    it("the user is part of the group", () => {
      expect(result.userIsMember).toBe(false);
    });
  });

  describe("when useris not logged in", () => {
    let result;
    beforeEach(() => {
      useSession.mockReturnValue({ loggedIn: false });
      const group = {
        members: [{ webId: "bob" }],
      };
      result = useMembership(group);
    });
    it("the user is not logged in", () => {
      expect(result.loggedIn).toBe(false);
    });
    it("the user is not part of the group", () => {
      expect(result.userIsMember).toBe(false);
    });
  });
});
