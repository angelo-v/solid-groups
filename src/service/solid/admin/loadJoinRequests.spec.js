import data from "@solid/query-ldflex";
import { value, values } from "../../../test-utils/ldflex";
import loadJoinRequests from "./loadJoinRequests";
import loadJoinRequest from "./loadJoinRequest";

jest.mock("@solid/query-ldflex", () => ({}));
jest.mock("./loadJoinRequest");

describe("when loading join requests", () => {
  describe("of a group with empty inbox", () => {
    let result;
    beforeEach(async () => {
      data["https://group.example/group/1"] = {
        ldp_inbox: value("https://group.example/inbox/1/"),
      };
      data["https://group.example/inbox/1/"] = {
        ldp_contains: values([]),
      };
      result = await loadJoinRequests("https://group.example/group/1");
    });

    it("the result is an empty array", () => {
      expect(result).toEqual([]);
    });
  });

  describe("of a group with one file in inbox", () => {
    let result;
    beforeEach(async () => {
      data["https://group.example/group/1"] = {
        ldp_inbox: value("https://group.example/inbox/1/"),
      };
      data["https://group.example/inbox/1/"] = {
        ldp_contains: values([
          "https://group.example/inbox/1/join-request.ttl",
        ]),
      };
      loadJoinRequest.mockResolvedValue("join-request");
      result = await loadJoinRequests("https://group.example/group/1");
    });

    it("the result contains the request", () => {
      expect(result).toEqual(["join-request"]);
    });

    it("join request was loaded", () => {
      expect(loadJoinRequest).toHaveBeenCalledWith(
        "https://group.example/inbox/1/join-request.ttl"
      );
    });
  });

  describe("of a group with some files in inbox", () => {
    let result;
    beforeEach(async () => {
      data["https://group.example/group/1"] = {
        ldp_inbox: value("https://group.example/inbox/1/"),
      };
      data["https://group.example/inbox/1/"] = {
        ldp_contains: values([
          "https://group.example/inbox/1/join-request-1.ttl",
          "https://group.example/inbox/1/join-request-2.ttl",
          "https://group.example/inbox/1/join-request-3.ttl",
        ]),
      };
      loadJoinRequest.mockResolvedValueOnce("one");
      loadJoinRequest.mockResolvedValueOnce("two");
      loadJoinRequest.mockResolvedValueOnce("three");
      result = await loadJoinRequests("https://group.example/group/1");
    });

    it("the result contains the request", () => {
      expect(result).toEqual(["one", "two", "three"]);
    });

    it("join request was loaded", () => {
      expect(loadJoinRequest).toHaveBeenCalledWith(
        "https://group.example/inbox/1/join-request-1.ttl"
      );
      expect(loadJoinRequest).toHaveBeenCalledWith(
        "https://group.example/inbox/1/join-request-2.ttl"
      );
      expect(loadJoinRequest).toHaveBeenCalledWith(
        "https://group.example/inbox/1/join-request-3.ttl"
      );
    });
  });

  describe("of a group with some files in inbox and one failing to load", () => {
    let result;
    beforeEach(async () => {
      data["https://group.example/group/1"] = {
        ldp_inbox: value("https://group.example/inbox/1/"),
      };
      data["https://group.example/inbox/1/"] = {
        ldp_contains: values([
          "https://group.example/inbox/1/join-request-1.ttl",
          "https://group.example/inbox/1/join-request-2.ttl",
          "https://group.example/inbox/1/join-request-3.ttl",
        ]),
      };
      loadJoinRequest.mockResolvedValueOnce("one");
      loadJoinRequest.mockRejectedValue(new Error("faulty"));
      loadJoinRequest.mockResolvedValueOnce("three");
      result = await loadJoinRequests("https://group.example/group/1");
    });

    it("the result contains the successfully loaded requests", () => {
      expect(result).toEqual(["one", "three"]);
    });
  });
});
