import React from "react";
import Snackbar from "@material-ui/core/Snackbar";
import Alert from "@material-ui/lab/Alert";

export const UserFeedback = ({open, message, onClose, severity}) => {
  return (
    <Snackbar
      anchorOrigin={{ vertical: "top", horizontal: "center" }}
      open={open}
    >
      {open ? <Alert onClose={onClose} severity={severity}>
        {message}
      </Alert> : <div/>}
    </Snackbar>
  );
};
