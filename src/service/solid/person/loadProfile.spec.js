import data from "@solid/query-ldflex";
import { missing, value } from "../../../test-utils/ldflex";
import loadProfile from "./loadProfile";

jest.mock("@solid/query-ldflex", () => ({}));

describe("when loading a profile", () => {
  describe("successfully with all vcard data available", () => {
    let user;
    beforeEach(async () => {
      data["https://alice.example/profile/card#me"] = {
        vcard_fn: value("Alice"),
        vcard_hasPhoto: value("https://alice.example/profile/photo.jpg"),
        ldp_inbox: value("https://alice.example/inbox/"),
      };
      user = await loadProfile("https://alice.example/profile/card#me");
    });

    it("the result contains the webId", () => {
      expect(user.webId).toBe("https://alice.example/profile/card#me");
    });

    it("the result contains the name", () => {
      expect(user.name).toBe("Alice");
    });

    it("the result contains the inbox", () => {
      expect(user.inbox).toBe("https://alice.example/inbox/");
    });

    it("the result contains the picture", () => {
      expect(user.imageSrc).toBe("https://alice.example/profile/photo.jpg");
    });
  });

  describe("successfully with all foaf data available", () => {
    let user;
    beforeEach(async () => {
      data["https://alice.example/profile/card#me"] = {
        vcard_fn: missing(),
        vcard_hasPhoto: missing(),
        foaf_name: value("Alice"),
        foaf_img: value("https://alice.example/profile/photo.jpg"),
        ldp_inbox: value("https://alice.example/inbox/"),
      };
      user = await loadProfile("https://alice.example/profile/card#me");
    });

    it("the result contains the webId", () => {
      expect(user.webId).toBe("https://alice.example/profile/card#me");
    });

    it("the result contains the name", () => {
      expect(user.name).toBe("Alice");
    });

    it("the result contains the picture", () => {
      expect(user.imageSrc).toBe("https://alice.example/profile/photo.jpg");
    });
  });
});
