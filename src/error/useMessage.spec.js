import { useMessage } from "./useMessage";
import { act } from "react-dom/test-utils";
import { withHook } from "../test-utils";

describe("use message", () => {
  let renderCycle;
  let render;
  beforeEach(() => {
    ({ render, renderCycle } = withHook(useMessage));
  });

  describe("before loading started", () => {
    it("is closed", () => {
      render();
      expect(renderCycle[0].open).toBe(false);
    });

    describe("but when error occurs", () => {
      it("opens and shows error", () => {
        render();
        act(() => {
          renderCycle[0].asyncStateChanged({
            loading: false,
            error: new Error("now something happened"),
          });
        });
        expect(renderCycle[1].open).toBe(true);
        expect(renderCycle[1].message).toBe("now something happened");
        expect(renderCycle[1].severity).toBe("error");
      });
    });

    describe("but when loading finished successfully", () => {
      it("opens and shows message", () => {
        render();
        act(() => {
          renderCycle[0].asyncStateChanged({
            loading: false,
            value: { message: "Great!" },
          });
        });
        expect(renderCycle[1].open).toBe(true);
        expect(renderCycle[1].message).toBe("Great!");
        expect(renderCycle[1].severity).toBe("success");
      });
    });
  });

  describe("with error", () => {
    beforeEach(() => {
      render();
      act(() => {
        renderCycle[0].asyncStateChanged({
          loading: false,
          error: new Error("it went bad"),
        });
      });
    });

    it("is opened", () => {
      expect(renderCycle[1].open).toBe(true);
    });
    it("shows error message", () => {
      expect(renderCycle[1].message).toBe("it went bad");
    });
    it("severity is error", () => {
      expect(renderCycle[1].severity).toBe("error");
    });
    describe("when closed", () => {
      it("is not opened anymore", () => {
        act(() => {
          renderCycle[1].closed();
        });
        expect(renderCycle[2].open).toBe(false);
      });
    });
  });

  describe("with successful result", () => {
    beforeEach(() => {
      render();
      act(() => {
        renderCycle[0].asyncStateChanged({
          loading: false,
          value: { message: "Great!" },
        });
      });
    });

    it("is opened", () => {
      render();
      expect(renderCycle[1].open).toBe(true);
    });
    it("shows success message", () => {
      render();
      expect(renderCycle[1].message).toBe("Great!");
    });

    it("severity is success", () => {
      render();
      expect(renderCycle[1].severity).toBe("success");
    });

    describe("when closed", () => {
      it("is not opened anymore", () => {
        render();
        act(() => {
          renderCycle[1].closed();
        });
        expect(renderCycle[2].open).toBe(false);
      });
    });
  });
});
