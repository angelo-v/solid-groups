import { useState } from "react";
import { useAsyncFn } from "react-use";
import { putConfig } from "./putConfig";
import { initializeGroupUsing } from "./initializeGroup";
import { useServiceContext } from "../service";
import { useUserFeedback } from "../error";

export const useSetupGroupForm = () => {
  const service = useServiceContext();
  const initializeGroup = initializeGroupUsing(service);
  const [uri, setUri] = useState(
    `${window.location.protocol}//${window.location.host}/groups/main#we`
  );

  const [submitState, onSubmit] = useAsyncFn(async () => {
    await initializeGroup(uri);
    await putConfig(uri);
    window.location.reload();
    return { message: "Congrats! Your group has been set up." };
  }, [uri, initializeGroup]);

  useUserFeedback(submitState);

  return {
    uri,
    onUriChange: (uri) => setUri(uri),
    onSubmit,
    submitState,
  };
};
