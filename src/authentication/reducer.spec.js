import reducer, {login, logout} from "./reducer";

describe("authentication reducer", () => {
  describe("when login action", () => {
    let session;
    beforeEach(() => {
      const state = {};
      const action = login({
        name: "John",
      });
      session = reducer(state, action);
    });

    it("then user is logged in", () => {
      expect(session.loggedIn).toBe(true);
    });

    it("then user data is present", () => {
      expect(session.user).toEqual({
        name: "John",
      });
    });
  });

  describe("when logout action", () => {
    let session;
    beforeEach(() => {
      const state = {
        loggedIn: true,
        user: { name: "John" },
      };
      const action = logout();
      session = reducer(state, action);
    });

    it("then user is logged out", () => {
      expect(session.loggedIn).toBe(false);
    });

    it("then user data is anonymous", () => {
      expect(session.user).toEqual({
        name: "Anonymous",
      });
    });
  });
});
