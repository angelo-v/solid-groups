import React from "react";
import { shallow } from "enzyme";
import { SetupGroup } from "./SetupGroup";
import { SetupGroupForm } from "./SetupGroupForm";
import { Login } from "./Login";
import { useSession } from "../authentication";
import config from "../config";
import { Redirect } from "react-router-dom";

jest.mock("../authentication");
jest.mock("../config", () => ({}));

describe("SetupGroup", () => {
  it("should show setup form when logged in", () => {
    useSession.mockReturnValue({
      loggedIn: true,
    });
    const result = shallow(<SetupGroup />);
    expect(result).toContainReact(<SetupGroupForm />);
    expect(result).not.toContainReact(<Login />);
  });

  it("should show login, when not logged in", () => {
    useSession.mockReturnValue({
      loggedIn: false,
    });
    const result = shallow(<SetupGroup />);
    expect(result).toContainReact(<Login />);
    expect(result).not.toContainReact(<SetupGroupForm />);
  });
  it("redirects to group page when already set up", () => {
    config.uri = "https://group.example/";
    const result = shallow(<SetupGroup />);
    expect(result).toContainReact(<Redirect to="/" />);
  });
});
