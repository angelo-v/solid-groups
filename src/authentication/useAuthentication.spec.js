import { useAuthentication } from "./useAuthentication";
import { act } from "react-dom/test-utils";
import { ANONYMOUS } from "./reducer";
import { withHook } from "../test-utils";

describe("useAuthentication", () => {
  let renderCycle = [];
  let render;
  const jane = { name: "Jane" };
  let authService;
  let fakeLogin, fakeLogout;
  beforeEach(() => {
    renderCycle = [];
    authService = {
      login: jest.fn(),
      logout: jest.fn(),
      trackSession: (onLogin, onLogout) => {
        fakeLogin = onLogin;
        fakeLogout = onLogout;
      },
    };
    ({ render, renderCycle } = withHook(useAuthentication, authService));
  });

  describe("login", () => {
    beforeEach(async () => {
      act(() => {
        render();
        renderCycle[0].login();
      });
    });
    it("calls auth service login", () => {
      expect(authService.login).toHaveBeenCalled();
    });
  });

  describe("logout", () => {
    beforeEach(async () => {
      act(() => {
        render(authService);
        renderCycle[0].logout();
      });
    });
    it("calls auth service logout", () => {
      expect(authService.logout).toHaveBeenCalled();
    });
  });

  describe("when user logs in and then out", () => {
    beforeEach(async () => {
      act(() => {
        render(authService);
      });

      await act(async () => {
        await fakeLogin(jane);
      });
      await act(async () => {
        await fakeLogout();
      });
    });

    it("session refers to anonymous user initially", () => {
      expect(renderCycle[0].user).toEqual(ANONYMOUS);
    });

    it("should not be logged in initially", () => {
      expect(renderCycle[0].loggedIn).toEqual(false);
    });

    it("session contains user after login", () => {
      expect(renderCycle[1].user).toEqual(jane);
    });

    it("should be logged in after login", () => {
      expect(renderCycle[1].loggedIn).toEqual(true);
    });

    it("session refers to anonymous user after logout", () => {
      expect(renderCycle[2].user).toEqual(ANONYMOUS);
    });

    it("should not be logged in after logout", () => {
      expect(renderCycle[2].loggedIn).toEqual(false);
    });
  });
});
