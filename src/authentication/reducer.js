export function login(user) {
  return {
    type: "login",
    payload: user,
  };
}

export function logout() {
  return {
    type: "logout",
  };
}

export const ANONYMOUS = {
  name: "Anonymous",
};

export default (session, { type, payload }) => {
  switch (type) {
    case "login":
      return {
        loggedIn: true,
        user: payload,
      };
    case "logout":
      return {
        loggedIn: false,
        user: ANONYMOUS,
      };
    default:
      throw Error();
  }
};
