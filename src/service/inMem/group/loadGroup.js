export const members = [
  { webId: "https://alice.example" },
  { webId: "https://bob.example" },
  { webId: "https://claire.example" },
];

export async function loadGroup(uri) {
  return {
    uri,
    name: "Fake Group",
    inbox: "inbox",
    members,
  };
}
