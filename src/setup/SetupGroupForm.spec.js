import React from "react";
import {useSetupGroupForm} from "./useSetupGroupForm";
import {shallow} from "enzyme";
import {SetupGroupForm} from "./SetupGroupForm";
import {AsyncButton} from "./AsyncButton";

jest.mock("./useSetupGroupForm");

describe("SetupGroupForm", () => {
  it("uri field contains uri as value", () => {
    useSetupGroupForm.mockReturnValue({
      uri: "https://uri.example",
      submitState: {}
    });

    const result = shallow(<SetupGroupForm />);
    expect(result.find('[name="uri"]')).toHaveValue("https://uri.example");
  });

  it("uri field should call uri change handler with dom value", () => {
    const onUriChange = jest.fn();
    useSetupGroupForm.mockReturnValue({
      onUriChange,
      submitState: {}
    });

    const result = shallow(<SetupGroupForm />);
    result
      .find('[name="uri"]')
      .simulate("change", { target: { value: "new value" } });
    expect(onUriChange).toHaveBeenCalledWith("new value");
  });

  it("should submit form and prevent default", () => {
    const onSubmit = jest.fn();
    const preventDefault = jest.fn();
    useSetupGroupForm.mockReturnValue({
      onSubmit,
      submitState: {}
    });

    const result = shallow(<SetupGroupForm />);

    result.find("form").simulate("submit", {preventDefault});
    expect(onSubmit).toHaveBeenCalled();
    expect(preventDefault).toHaveBeenCalled();
  });

  it('should indicate loading', () => {
    useSetupGroupForm.mockReturnValue({
      submitState: {
        loading: true
      },
    });
    const result = shallow(<SetupGroupForm />);
    const button = result.find(AsyncButton);
    expect(button).toHaveProp('loading', true);
  });

});
