import {useAsync} from "react-use";
import {useServiceContext} from "../service";

export const useGroup = (uri) => {
  const { loadGroup } = useServiceContext();
  const { value, loading, error } = useAsync(() => loadGroup(uri), [uri]);

  const group = {
    name: "Unnamed Group",
    ...value,
  };

  return {
    group,
    loading,
    error,
  };
};
